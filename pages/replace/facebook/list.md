---
title: Ciao Facebook
subtitle: Recommended Social Networks

content:
    items: 
        - '@taxonomy.replaces': facebook
    order:
        by: default
        custom:
            - friendica
            - mastodon

hero_classes: hero-tiny title-h1h2
---