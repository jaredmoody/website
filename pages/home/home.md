---
title: switching.software
subtitle: Ethical, easy-to-use and privacy-conscious alternatives to well-known software

content:
    items: 
        - '@page.children': /replace
        - '@page.self': /about
        - '@page.self': /about/switching
    order:
        by: default
        custom:
            - facebook
            - whatsapp
            - twitter
            - instagram
            - youtube
            - google-maps

body_classes: title-center title-h1h2
hero_classes: hero-tiny title-h1h2
---