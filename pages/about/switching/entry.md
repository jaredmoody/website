---
title: Guide to Switching
subtitle: Hints for a lasting change
body_classes: title-center title-h1h2
---

Some important things to remember when you’re switching to privacy-friendly alternatives:

## You don’t need to be a computer nerd

Lots of alternatives are easy to use, and the aim of switching.social is to only list these kinds of alternatives.

## You don’t need to switch everything all at once

The idea of switching is to permanently move to more privacy-friendly services, and that can take a bit of time to do correctly. It’s better to switch in a sustainable way than to have a bad experience switching.

Many people have a transition period where they use both old and new services simultaneously, or try out different alternatives before settling on one of them.

## When you switch, tell your friends

The more visibility alternatives have, the more likely people are to try them out. You don’t need to preach, but simply saying that you’re using a particular service helps the service get onto people’s radar.

## Switch what you can

Not everyone can find alternatives that suit their needs, so it’s ok to just switch what you can.

New alternatives are being developed all the time, and if there’s nothing that suits you right now there may be something suitable already in the pipeline (take a look at “Bubbling Under” for some examples).

## If you need help switching, ask for help

If you don’t know anyone who can offer advice on alternatives, you can contact this site via Email, Mastodon or Twitter.

## Help others who need help switching

If you want to protect your own privacy, it’s a good idea to protect other people’s privacy too.

For example, if someone installs the Facebook app, it will read the contacts list on their smartphone and send a copy of it back to Facebook. This will breach the privacy of everyone listed in the contacts, even if they aren’t on Facebook. Helping your friends switch to better alternatives is good for their privacy and good for yours too.

Also, the more people switch to alternatives, the better those alternatives become. As more people use alternative social networks, those networks become more attractive to new members, and at some point they can snowball into the default choice. (This is the case with email for example, which is the default way of messaging on the internet despite no one promoting or owning the email network.)

If someone asks for help getting away from Facebook, Google or wherever, do what you can to help them. Send them a link to switching.social and/or other sites like it.

## Don’t scare people who don’t want to be scared

Some websites that discuss internet privacy are quite frightening, and in a way they have good reason to be because some dangerous things really are happening.

However, not everyone wants to hear about the threat of mass surveillance when they’re eating their cornflakes and already overwhelmed with concerns about their job/family/bills etc.

A positive, non-scary approach may sometimes be a better way of letting people know about alternatives. For example, many people have observed that Mastodon is a lot more friendly than Twitter or Facebook.